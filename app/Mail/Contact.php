<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Name of the person sending the mail
     *
     * @var string
     */
    public $name;

    /**
     * Email who the mail is from
     *
     * @var string
     */
    public $email;

    /**
     * WHat the person said
     *
     * @var string
     */
    public $message;


    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct(string $name, string $email, string $message)
    {
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.contact')->with(
        [
            'name' => $this->name,
            'email' => $this->email,
            'content' => $this->message,
        ]);
    }
}
