<?php

namespace App\GraphQL\Mutations;

use App\Http\Controllers\ContactController;

class CreateContact
{
    public function resolve($root, array $args)
    {
        $name = $args['name'];
        $email = $args['email'];
        $message = $args['message'];
        
        return ContactController::contact($name, $email, $message);
    }
}
