<?php

namespace App\Http\Middleware;

class CorsMiddleware
{
    public function handle($request, \Closure $next)
    {
        // return OK when OPTIONS method is given
        // some clients first send an OPTIONS request
        // for :umen rejects this.
        $response = ($request->isMethod('OPTIONS'))
            ? response('', 200)
            : $response = $next($request);

        // set response headers
        $response->header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
        $response->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));
        $response->header('Access-Control-Allow-Origin', '*');
        
        return $response;
    }
}
