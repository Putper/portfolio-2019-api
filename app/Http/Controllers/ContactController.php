<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\Contact as ContactMail;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){}

    
    public static function contact(string $name, string $email, string $message)
    {
        $email_to = config('mail.admin.address');
        $template = new ContactMail($name, $email, $message);

        try
        {
            Mail::to($email_to)->send($template);
        }
        catch(\Exception $exception)
        {
            report($exception);
            return false;
        }

        return true;
    }
}
