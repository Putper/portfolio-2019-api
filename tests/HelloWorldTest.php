<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use KunicMarko\GraphQLTest\Operation\Query;

class HelloWorldTest extends TestCase
{
    /**
     * Test hello world
     *
     * @return void
     */
    public function testReadHelloWorld(): void
    {
        $query = $this->query(
            new Query('hello')
        );
        $result = json_encode($query);

        $expected = json_encode(['hello' => 'world!']);
        
        $this->assertContains($expected, $result);
    }
}
