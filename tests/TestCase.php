<?php

use KunicMarko\GraphQLTest\Bridge\Lumen\TestCase as TestCaseBase;

abstract class TestCase extends TestCaseBase
{
    public function setUp(): void
    {
        Parent::setUp();
        
        $this->setDefaultHeaders([
            'Content-Type' => 'application/json',
        ]);
    }


    /**
     * Creates the application.
     *
     * @return Laravel\Lumen\Application
     */
    public function createApplication(): Laravel\Lumen\Application
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
}
