<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use KunicMarko\GraphQLTest\Operation\Mutation;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contact as ContactMail;

class CreateContactTest extends TestCase
{
    /**
     * Test hello world
     *
     * @return void
     */
    public function testCreateContact(): void
    {
        // prevent mail from being sent
        Mail::fake();

        // assert no mails were sent
        Mail::assertNothingSent();

        $name = "Jonny Cruz";
        $email = "mail@example.com";
        $message = "Vamos esculachar!";
        
        // perform request
        $mutation = $this->mutation(
            new Mutation(
                'createContact',
                [
                    'name' => $name,
                    'email' => $email,
                    'message' => $message
                ]
            )
        );
        $result = json_encode($mutation);

        // assert expected result: returned true
        $expected = json_encode(['createContact' => true]);
        $this->assertContains($expected, $result);

        // assert mail variables
        Mail::assertSent(ContactMail::class, function($mail) use($name, $email, $message)
        {
            return $mail->name === $name
                && $mail->email === $email
                && $mail->message === $message;
        });

        // assert mail was sent to the right address
        $mail_to = config('mail.admin.address');
        Mail::assertSent(ContactMail::class, function($mail) use($mail_to)
        {
            return $mail->hasTo($mail_to);
        });
    }
}
