<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class HomeTest extends TestCase
{
    /**
     * Test homepage contains all repositories
     *
     * @return void
     */
    public function testHomeHasRepository()
    {
        $this->get('/')->seeStatusCode(200);
        $links = config('app.links');

        foreach($links as $link)
        {
            $this->assertContains(
                $link, $this->response->getContent()
            );
        }
    }
}
