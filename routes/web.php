<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function() use($router)
{
    $links = config('app.links');
    $html = '';

    // turn links into <a> links
    foreach( $links as $name => $link )
    {
        $html .= "<b>{$name}: </b>";
        $html .= "<a target='_BLANK' href='{$link}'>{$link}</a>";
        $html .= "<br/>";
    }
    $html = "<html>{$html}</html>";
    return response($html, 200)->header('Content-Type', 'text/html');
});
