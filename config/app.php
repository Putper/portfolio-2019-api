<?php

return [
    'links' => [
        'API Source' => 'https://gitlab.com/Mantik/portfolio-2019-api',
        'Client Source' => 'https://gitlab.com/Mantik/portfolio-2019-site',
    ],
];
