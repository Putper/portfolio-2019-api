# Portfolio API
For my personal portfolio, built in [Lumen](https://lumen.laravel.com/).  
[Client source](https://gitlab.com/Mantik/portfolio-2019-site/)  
[API Source](https://gitlab.com/Mantik/portfolio-2019-api/)  


## Setup
1. copy `.env.example` and name it `.env`, then set all variables in this file.
   ```bash
   $ cp .env.example .env
   ```
2. Install dependencies
   ```bash
   $ composer install
   ```
3. Serve the application through the `./public` directory.  
For serving locally you can run:
   ```bash
   $ php -S localhost:8000 -t public/
   ```


## Run Unit Tests
```bash
$ composer test
```
